// Part 1: Start
fetch("https://jsonplaceholder.typicode.com/todos")
.then((res) => res.json())
.then((res) => console.log(Array = res.map((data) => data.title)));
// Part 1: End


// Part 2: Start
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => res.json())
.then((data) => console.log(`${data.title}: ${data.completed}`));
// Part 2: End

fetch("https://jsonplaceholder.typicode.com/todos", {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({
        "userID": 3,
        "title": "S33 Activity",
        "completed": true
    })
})
.then(res => res.json())
.then(res => console.log(res));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: 'PUT',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({
        "title": "Create todo list item",
        "completed": false,
        "userId": 1
    })
})
.then(res => res.json())
.then(res => console.log(res));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: 'PATCH',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({
        "dateCompleted": "02/23/2023",
        "userId": 1,
        "title": "Update to do list item",
        "description": "To update to my to do list with a different data structure"
    })
})
.then(res => res.json())
.then(res => console.log(res));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: 'DELETE'
});